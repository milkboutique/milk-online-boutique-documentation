# Milk Online Boutique Documentation

Useful resources for Milk Boutique employees to better understand the site implementation.

Simple Pre-Order App Guide (for Milk Online Boutique Managers)
==========
Simple Pre-Order is the app that Milk Boutique uses to enable pre-ordering for specific items.  Pre-Ordering is DISABLED by default — so new products that are added WILL NOT have this functionality unless an online boutique manager enables the functionality specifically for that product.

Similarly all previously existing products must have Pre-Ordering enabled — only OUT OF STOCK items can be configured as 'Pre-Order'-able.

To enable Pre-Ordering
-------------
To enable pre-ordering of a specific product online boutique managers should visit the [Simple Pre-Order Shopify app](https://milk-boutique.myshopify.com/admin/apps/simple-pre-order) from within the 'Apps' tab of the online boutique Administrator area — or by [clicking here](https://milk-boutique.myshopify.com/admin/apps/simple-pre-order).

![Enable Pre-Order Functionality](https://trello-attachments.s3.amazonaws.com/5bc8f508ff102e449b6da14c/600x335/a36a8e7fa80e93781cb34888da74c0e4/Screen_Shot_2018-10-31_at_11.55.28_AM.png)

Once there click on 'Products' in order to visit the Pre-Order Enable view which will allow you to search for ONLY out-of-stock products — which can then be enabled as Pre-Orders.

Keep in mind that these products must ALSO be enabled / available through the online-store sales channel which can be done from each Product's product management page (ie. where you would edit the product description etc).  

To enable a product for sale through the online store sales channel visit the products page and look for the 'Product Availability' section which may look like this (top right depending on screen size — or alternately may site under the SEO section below images):


![Product Availability for Online Store Channel](https://trello-attachments.s3.amazonaws.com/5babe5e6e4aa845c7d12878b/5bc8f508ff102e449b6da14c/dc849d5ae3bd269d3555f26642c87867/Screen_Shot_2018-10-31_at_12.17.28_PM.png)

As mentioned the Product Availability - online store channel setting - location may differ depending on your device screen size. 

View / Manage Pre-Orders that have been Placed
--------------
Clicking on [Simple Pre-Order Shopify app](https://milk-boutique.myshopify.com/admin/apps/simple-pre-order) from within the 'Apps' tab will land you by default on the Pre-Order management page.

This allows you to manage Pre-Orders received from customers and manage those orders once the products are in-stock.

Pre-Order Trouble Shooting & Frequently Asked Questions
================
Generally the Pre-Order functionality is relatively difficult to break — and relatively low risk to the store in general — for that reason we encourage all online boutique managers to attempt to work with the Pre-Order functionality themselves first (rather than escalating to tech  developers) as most problems are easy to fix.

If you run into issues, check the following FAQ for a solution:

#Frequently Asked Quesitons
#### I have enabled a product for Pre-Ordering but I can't find it on the Online Boutique — Why?
In order to be Pre-Order-able a product must be all of the following:

1. Out-of-Stock — Inventory must equal ZERO — set via each product's individual [Product Administration section](https://milk-boutique.myshopify.com/admin/products) of the online boutique admin area.
2. Enabled for Sale in Online Store Sales Channel — set via each product's individual [Product Administration section](https://milk-boutique.myshopify.com/admin/products) of the online boutique admin area.
3. Have Pre-Ordering Enabled via the Simple Pre-Order Shopify App

The two pieces that are most easily forgotten / missed are #1 and #2 — so be sure to check that the product is marked Out-of-Stock (inventory = 0) AND enabled for sale via Online Store Sales Channel — which are both set from the [Product Administration section](https://milk-boutique.myshopify.com/admin/products).

####  Why can't I see the Product I want to enable in Simple Pre-Order?
As mentioned ONLY out-of-stock items can be enabled as pre-orderable.  This means that if the product you want to ENABLE is not visible from the Simple Pre-Order App — that likely means that the stock for that item is not set to ZERO.

To enable Pre-Ordering for that item you must FIRST set the inventory amount to Zero in shopify (ideally this is synced to Springboard Retail inventory management system).  Once set to zero you can then re-visit the Simple Pre-Order App here — and search for the desired product name — in order to enable Pre-Ordering.


####  Why is the 'Add to Cart' button still there even after I enabled Pre-Ordering?
Since the entire Pre-Order system functions ONLY WHEN stock levels are zero — adding any stock of an item that was previously Pre-Order-able will disable the functionality and change the Button Text back to the default online shop text: 'Add to Cart'.  

For this reason if a product SHOULD be Pre-Order-able, but is instead showing the default 'Add to Cart' text, the first thing that should be done to trouble shoot this behavior is to CHECK STOCK levels on that product.  If the stock is more than ZERO the product will default to 'Add to Cart'.

Official App Support — Requesting Bug / Error Support from App Creator
=========
If necessary Milk Web Developers / Web Professionals can request official support.  Online Boutique Managers SHOULD NOT pursue this avenue and should first escalate their problems to Milk Web Developers if boutique managers believe there is an error in the Pre-Order Shopify App.

### For Web Developer Reference
Milk Boutique Pre-Order functionality is based on the following Shopify App — and Milk's paid subscription enables us to request support from the App creator from the app's home page which is found here: 
https://apps.shopify.com/simple-pre-order?surface_detail=back+order&surface_inter_position=1&surface_intra_position=11&surface_type=search